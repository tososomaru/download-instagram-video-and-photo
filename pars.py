import requests
import os
import time
import urllib

USERNAME = []


def imageDownloader(edge, images_path):
    """ Downloads images """
    display_url = edge['node']['display_url']
    file_name = edge['node']['taken_at_timestamp']
    download_path = f"{images_path}\\{file_name}.jpg"
    if os.path.exists(download_path):
        return
    urllib.request.urlretrieve(display_url, download_path)


def videoDownloader(shortcode, videos_path):
    """ Downloads videos """
    videos = requests.get(f"https://www.instagram.com/p/{shortcode}/?__a=1")
    video_url = videos.json()['graphql']['shortcode_media']['video_url']
    file_name = videos.json()['graphql']['shortcode_media']['taken_at_timestamp']
    download_path = f"{videos_path}\\{file_name}.mp4"
    if os.path.exists(download_path):
        return
    urllib.request.urlretrieve(video_url, download_path)


def sidecarDownloader(shortcode, images_path, videos_path):
    """ Downloads images and videos from posts containing more than one pictures or videos """
    r = requests.get(f"https://www.instagram.com/p/{shortcode}/?__a=1")
    for i, edge in enumerate(r.json()['graphql']['shortcode_media']['edge_sidecar_to_children']['edges']):
        if edge['node']['is_video']:
            url = edge['node']['video_url']
            media_type = 'mp4'
            path = videos_path
        else:
            url = edge['node']['display_url']
            media_type = 'jpg'
            path = images_path
        file_name = r.json()['graphql']['shortcode_media']['taken_at_timestamp']
        download_path = f"{path}\\{file_name}_{i}.{media_type}"
        if os.path.exists(download_path):
            return
        urllib.request.urlretrieve(url, download_path)


LOADER = {'GraphImage': imageDownloader, 'GraphVideo': videoDownloader, 'GraphSidecar': sidecarDownloader}


def loading_media(graphql, images_path, videos_path):
    for edge in graphql['user']['edge_owner_to_timeline_media']['edges']:
        typename = edge['node']['__typename']
        shortcode = edge['node']['shortcode']
        if typename in LOADER.keys():
            LOADER_ARG = {'GraphImage': [edge, images_path],
                          'GraphVideo': [shortcode, videos_path],
                          'GraphSidecar': [shortcode, images_path, videos_path]}

            LOADER[f"{typename}"](*LOADER_ARG[f"{typename}"])


def create_dir_for_images_and_videos(path):
    imagesPath = f"{path}\\Images"
    videosPath = f"{path}\\Videos"
    if os.path.exists(path):
        return imagesPath, videosPath
    os.makedirs(path)
    os.makedirs(imagesPath)
    os.makedirs(videosPath)
    return imagesPath, videosPath


def parse(url, path):
    """ Runs methods that download photos/videos from the user """
    user_id = requests.get(url).json()['graphql']['user']['id']
    end_cursor = ''
    next_page = True
    images_path, videos_path = create_dir_for_images_and_videos(path)

    while next_page:
        r = requests.get(
            'https://www.instagram.com/graphql/query/',
            params={
                'query_id': '17880160963012870',
                'id': user_id,
                'first': 12,
                'after': end_cursor
            }
        )
        graphql = r.json()['data']

        loading_media(graphql, images_path, videos_path)

        end_cursor = graphql['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']
        next_page = graphql['user']['edge_owner_to_timeline_media']['page_info']['has_next_page']
        time.sleep(10)


def main():
    for user in USERNAME:
        account_url = "https://www.instagram.com/{}/?__a=1".format(user)
        path = user
        parse(account_url, path)


if __name__ == "__main__":
    main()
